<?php

class frog
{

    public $name;
    public $legs = 4;
    public $cold_blooded = 'no';
    public $jump;

    public function __construct($string)
    {
        $this->name = $string;
    }

    public function jump()
    {

        return $this->jump = 'Hop - hop';
    }
}
