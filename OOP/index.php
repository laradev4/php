<?php

include('shaun.php');
include('frog.php');
include('ape.php');
$shaun = new shaun('Shaun The sheep');

echo 'Name : ' . $shaun->name . '<br>';
echo 'Legs : ' . $shaun->legs . '<br>';
echo 'Cold Blooded : ' . $shaun->cold_blooded . '<br><br>';

$frog = new frog('Kodok');

echo 'Name : ' . $frog->name . '<br>';
echo 'Legs : ' . $frog->legs . '<br>';
echo 'Cold Blooded : ' . $frog->cold_blooded . '<br>';
echo 'Jump : ' . $frog->jump('Hop - hop') . '<br><br>';

$ape = new ape('Kera Sakti');

echo 'Name : ' . $ape->name . '<br>';
echo 'Legs : ' . $ape->legs . '<br>';
echo 'Cold Blooded : ' . $ape->cold_blooded . '<br>';
echo 'Jump : ' . $ape->yell() . '<br>';
